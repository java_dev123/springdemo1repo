package com.myspringprojects.demo.repository;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.data.repository.query.Param;
import com.myspringprojects.demo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	List<Employee> findByLastName(@Param("name") String name);

	List<Employee> findBySalaryGreaterThan(@Param("salary") BigDecimal salary);

}
