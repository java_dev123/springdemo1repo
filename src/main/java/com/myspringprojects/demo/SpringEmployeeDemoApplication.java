package com.myspringprojects.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringEmployeeDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringEmployeeDemoApplication.class, args);
	}

}
