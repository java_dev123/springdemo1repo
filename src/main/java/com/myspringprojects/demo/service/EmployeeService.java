package com.myspringprojects.demo.service;

import java.math.BigDecimal;
import java.util.List;

import com.myspringprojects.demo.exception.ResourceNotFoundException;
import com.myspringprojects.demo.model.Employee;

/**
 * 
 * This is the service interface. In Spring framework the service interface is
 * optional but it is good to have it to get the benefit from the interface's
 * features
 */
public interface EmployeeService {

	List<Employee> getAllEmployees();

	List<Employee> getEmployeesByLastName(String name);

	List<Employee> getEmployeesSalaryGreaterThan(BigDecimal salary);

	Employee getEmployeeById(Long id) throws ResourceNotFoundException;

	Employee updateEmployee(Employee entity) throws ResourceNotFoundException;

	void deleteEmployeeById(Long id) throws ResourceNotFoundException;

	Employee addNewEmployee(Employee employee);
}