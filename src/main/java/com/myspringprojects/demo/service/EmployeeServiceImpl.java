package com.myspringprojects.demo.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.myspringprojects.demo.exception.ResourceNotFoundException;
import com.myspringprojects.demo.model.Employee;
import com.myspringprojects.demo.repository.EmployeeRepository;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 
 * This class is a Service implementation class (for the EmployService
 * interface)
 *
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository repository;

	@Override
	public List<Employee> getAllEmployees() {
		List<Employee> employeeList = repository.findAll();

		if (employeeList.size() > 0) {
			return employeeList;
		} else {
			// if no record is found, just return a empty list
			return new ArrayList<Employee>();
		}
	}

	@Override
	public List<Employee> getEmployeesByLastName(String name) {
		List<Employee> employeeList = repository.findByLastName(name); // this function is actually provided by Spring

		if (employeeList.size() > 0) {
			return employeeList;
		} else {
			// if no record is found, just return a empty list
			return new ArrayList<Employee>();
		}
	}

	@Override
	public List<Employee> getEmployeesSalaryGreaterThan(BigDecimal salary) {
		List<Employee> employeeList = repository.findBySalaryGreaterThan(salary); // this function is provided by Spring

		if (employeeList.size() > 0) {
			return employeeList;
		} else {
			return new ArrayList<Employee>();
		}
	}

	@Override
	public Employee getEmployeeById(Long id) throws ResourceNotFoundException {
		Optional<Employee> employee = repository.findById(id);

		if (employee.isPresent()) {
			// if the employee is found, return it
			return employee.get();
		} else {
			// if this id doesn't exist , throw the ResourceNotFoundException exception
			throw new ResourceNotFoundException("No employee record is found for id:" + id);
		}
	}

	@Override
	public Employee addNewEmployee(@RequestBody Employee emp) {
		Optional<Employee> employee = repository.findById(emp.getId());
		if (employee.isPresent()) {
			/*
			 * when create a new employee, if user input an id which already exist, we will
			 * manually change the id to -1 to make it as a new employee (otherwise it will
			 * update the existing employ with that id). This is a workaround way for this
			 * demo
			 */
			emp.setId(Long.valueOf(-1));
			Employee newEmployee = repository.save(emp);
			return newEmployee;

		} else {
			Employee newEmployee = repository.save(emp);
			return newEmployee;
		}

	}

	@Override
	public Employee updateEmployee(Employee entity) throws ResourceNotFoundException {

		Optional<Employee> employee = repository.findById(entity.getId());
		if (employee.isPresent()) {
			// if this id exist, update this employee
			Employee newEntity = employee.get();
			newEntity.setJobTitle(entity.getJobTitle());
			newEntity.setFirstName(entity.getFirstName());
			newEntity.setLastName(entity.getLastName());
			newEntity.setSalary(entity.getSalary());

			newEntity = repository.save(newEntity);
			return newEntity;
		} else {
			// if this id doesn't exist , throw the ResourceNotFoundException exception
			throw new ResourceNotFoundException("This record doesn't exist, so it can't be updated");
		}

	}

	@Override
	public void deleteEmployeeById(Long id) throws ResourceNotFoundException {
		Optional<Employee> employee = repository.findById(id);

		if (employee.isPresent()) {
			repository.deleteById(id);
		} else {
			/*
			 * if this employee doesn't exist , throw the ResourceNotFoundException
			 * exception
			 */
			throw new ResourceNotFoundException("No employee is found for id:" + id + " , so it can't be deleted");
		}
	}

}
