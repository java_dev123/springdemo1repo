package com.myspringprojects.demo.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

import com.myspringprojects.demo.exception.ResourceNotFoundException;
import com.myspringprojects.demo.model.Employee;
import com.myspringprojects.demo.repository.EmployeeRepository;
import com.myspringprojects.demo.service.EmployeeService;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {

	@Autowired
	EmployeeService service;
	@Autowired
	EmployeeRepository repository;

	/**
	 * Get all employees
	 * 
	 * @return
	 */
	@GetMapping
	public ResponseEntity<List<Employee>> getAllEmployees() {
		List<Employee> list = service.getAllEmployees();

		return new ResponseEntity<List<Employee>>(list, new HttpHeaders(), HttpStatus.OK);
	}

	/**
	 * Get employee by id
	 * 
	 * @param id
	 * @return
	 * @throws ResourceNotFoundException
	 */
	@GetMapping("/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") Long id) throws ResourceNotFoundException {
		Employee entity = service.getEmployeeById(id);
		return new ResponseEntity<Employee>(entity, new HttpHeaders(), HttpStatus.OK);
	}

	/**
	 * Find employee(s) by last name
	 * 
	 * @param name
	 * @return
	 * @throws ResourceNotFoundException
	 */
	@GetMapping("/findby-lastname/{name}")
	public ResponseEntity<List<Employee>> getEmployeeByLastName(@PathVariable("name") String name)
			throws ResourceNotFoundException {
		List<Employee> list = service.getEmployeesByLastName(name);
		return new ResponseEntity<List<Employee>>(list, new HttpHeaders(), HttpStatus.OK);
	}

	/**
	 * Find employee(s) whose salary is greater than the input amount
	 * 
	 * @param salary
	 * @return
	 * @throws ResourceNotFoundException
	 */
	@GetMapping("/findby-salary-greater-than/{salary}")
	public ResponseEntity<List<Employee>> getEmployeeByLastName(@PathVariable("salary") BigDecimal salary)
			throws ResourceNotFoundException {
		List<Employee> list = service.getEmployeesSalaryGreaterThan(salary);
		return new ResponseEntity<List<Employee>>(list, new HttpHeaders(), HttpStatus.OK);
	}

	/**
	 * Update details of an employee
	 * 
	 * @param employee
	 * @return
	 * @throws ResourceNotFoundException
	 */
	@PutMapping(path = "/update", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee) throws ResourceNotFoundException {
		Employee updatedEmployee = service.updateEmployee(employee);
		return new ResponseEntity<Employee>(updatedEmployee, new HttpHeaders(), HttpStatus.OK);
	}

	/**
	 * Add new employee
	 * 
	 * @param employee
	 * @return
	 */
	@PostMapping(path = "/add-new-employee", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Employee> addNewEmployee(@RequestBody Employee employee)

	{
		Employee newEmployee = service.addNewEmployee(employee);
		return new ResponseEntity<Employee>(newEmployee, new HttpHeaders(), HttpStatus.OK);
	}

	/**
	 * Delete an employee by id
	 * 
	 * @param id
	 * @return
	 * @throws ResourceNotFoundException
	 */
	@DeleteMapping("/{id}")
	public HttpStatus deleteEmployeeById(@PathVariable("id") Long id) throws ResourceNotFoundException {
		service.deleteEmployeeById(id);
		return HttpStatus.OK;
	}

}