package com.myspringprojects.demo.exception;

import java.util.Date;

public class RestErrorResponse {

	private Date timestamp;
	private String errorDetails;
	private String status;
	private String message;

	public RestErrorResponse(Date timestamp, String status, String msg, String errorDetails) {
		this.status = status;
		this.message = msg;
		this.errorDetails = errorDetails;
		this.timestamp = timestamp;

	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetails() {
		return errorDetails;
	}

	public void setDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}