package com.myspringprojects.demo.exception;

import java.util.Date;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.WebRequest;
import org.slf4j.Logger;

/**
 * Exception handler Class in global level (via the ControlAdvice annotation),
 * so no try/catch block is needed in the code
 *
 */
@ControllerAdvice
public class RestExceptionHandler {

	/*
	 * because all the exception handling is centralized in this class, so the
	 * logger here can log the exception and the stack trace to the log file
	 */
	private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

	/**
	 * Exception Handler for the user-defined exception ResourceNotFoundException
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<?> resourceNotFoundExceptionHandler(ResourceNotFoundException ex, WebRequest request) {

		RestErrorResponse restEx = new RestErrorResponse(new Date(), HttpStatus.NOT_FOUND.toString(), ex.getMessage(),
				request.getDescription(false));
		logger.error("error for the ResourceNotFoundException: " + ex.getMessage(), ex);
		return new ResponseEntity<>(restEx, HttpStatus.NOT_FOUND);
	}

	/**
	 * Exception Handler for All other exceptions
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> otherExcpetionHandler(Exception ex, WebRequest request) {
		RestErrorResponse restExResponse = new RestErrorResponse(new Date(),
				HttpStatus.INTERNAL_SERVER_ERROR.toString(), ex.getMessage(), request.getDescription(false));
		logger.error("error for the general exception: " + ex.getMessage(), ex);
		return new ResponseEntity<>(restExResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}