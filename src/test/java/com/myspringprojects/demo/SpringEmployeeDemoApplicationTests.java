package com.myspringprojects.demo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.myspringprojects.demo.controller.EmployeeController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringEmployeeDemoApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private EmployeeController restController;

	/*
	 * Using the Spring's MockMvc test support to do the Junit test
	 */
	private MockMvc mvc;

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(restController).build();
	}

	@Test
	public void testGetEmployeeById() throws Exception {
		RequestBuilder builder = MockMvcRequestBuilders.get("/api/employees/1").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mvc.perform(builder).andReturn();
		System.out.println(result.getResponse().getContentAsString());
	}

}